import numpy as np
from numpy import cos, sin, sqrt, arccos, arcsin, arctan2, pi


def sign(a):
    return 1 if a > 0 else -1


class Quaternion:

    def __init__(self,w=1.,x=0.,y=0.,z=0.):
        self.Q = np.array([
            [w, -x, -y, -z],
            [x,  w, -z,  y],
            [y,  z,  w, -x],
            [z, -y,  x,  w]
        ], dtype='float')


    def normalized(self):
        return Quaternion(*(self.Q / self.linalg.norm(self.q[0])))


    def components(self):
        return np.reshape(self.Q[:,0], (-1))


    def vec(self):
        return self.Q[1:,0]


    def scal(self):
        return self.Q[0,0]


    def to_angleaxis(self):
        w,x,y,z = self.components()

        l = np.array([x,y,z])
        s = np.linalg.norm(l)
        if s == 0.:
            return 0., np.array([0.,0.,0.])

        l = l / s
        a = 2 * arctan2(s,w)
        return a, l


    @staticmethod
    def from_angleaxis(a, l):
        if np.linalg.norm(l) == 0.:
            return Quaternion(1,0,0,0)
        w = cos(a/2.)
        x,y,z = np.array(l) * sin(a/2.) / np.linalg.norm(l)
        return Quaternion(w,x,y,z)


    def to_rotmat(self):
        w,x,y,z = self.components()
        return np.array([
            [1 - 2*y**2 - 2*z**2,       2*x*y - 2*z*w,       2*x*z + 2*y*w],
            [      2*x*y + 2*z*w, 1 - 2*x**2 - 2*z**2,       2*y*z - 2*x*w],
            [      2*x*z - 2*y*w,       2*y*z + 2*x*w, 1 - 2*x**2 - 2*y**2]
        ])


    @staticmethod
    def from_rotmat(R):
        t = R[0,0] + R[1,1] + R[2,2]
        r = sqrt(1. + t)
        w = r / 2.
        x = sqrt(abs(1 + R[0,0] - R[1,1] - R[2,2])) * sign(R[2,1] - R[1,2]) / 2.
        y = sqrt(abs(1 - R[0,0] + R[1,1] - R[2,2])) * sign(R[0,2] - R[2,0]) / 2.
        z = sqrt(abs(1 - R[0,0] - R[1,1] + R[2,2])) * sign(R[1,0] - R[0,1]) / 2.
        return Quaternion(w,x,y,z)

        # TODO stable conversion
        # 
        # Rxx, Rxy, Rxz = R[0]
        # Ryx, Ryy, Ryz = R[1]
        # Rzx, Rzy, Rzz = R[2]
        # K = np.array([
        #     [Rxx - Ryy - Rzz,       Ryx + Rxy,       Rzx + Rxz,       Ryz - Rzy],
        #     [      Ryx + Rxy, Ryy - Rxx - Rzz,       Rzy + Ryz,       Rzx - Rxz],
        #     [      Rzx + Rxz,       Rzy + Ryz, Rzz - Rxx - Ryy,       Rxy - Ryx],
        #     [      Ryz - Rzy,       Rzx - Rxz,       Rxy - Ryx, Rxx + Ryy + Rzz]
        # ]) / 3.
        # eig_vals,eig_vects = np.linalg.eig(K)
        # i = np.argmax(eig_vals)
        # print eig_vects
        # print 'vects:', e_vects[0]

    def conj(self):
        quat = Quaternion()
        quat.Q = self.Q.T.copy()
        return quat


    def __mul__(q1, q2):
        q = Quaternion()
        q.Q = q1.Q.dot(q2.Q)
        return q


    def __add__(a1, a2):
        q = Quaternion()
        q.Q = q1.Q + q2.Q
        return q


    @staticmethod
    def from_rodrigues(rodrigues):
        n = np.linalg.norm(rodrigues)
        if n == 0.:
            return Quaternion(1,0,0,0)

        theta = n
        l = rodrigues / n
        return Quaternion.from_angleaxis(theta, l)


    def to_rodrigues(self):
        theta, l = self.to_angleaxis()
        return l * theta


    def norm(self):
        return self.Q.T.dot(self.Q)[0,0]


def make_homogen_trans(p, q):
    T = np.zeros((4,4))
    T[0:3,0:3] = q.to_rotmat()
    T[0:3,3] = np.reshape(p, (3,))
    T[3,3] = 1.
    return T


def decompose_homogen_trans(T):
    R = T[0:3,0:3]
    q = Quaternion.from_rotmat(R)
    p = T[0:3,3]
    return p,q


'''
    Tests
'''
def test_Q_to_R():
    np.set_printoptions(precision=4, suppress=True)

    rotation = 1., np.array([1.,2.,3.])
    Q = Quaternion.from_angleaxis(*rotation)
    R = Q.to_rotmat()
    Rinv = np.linalg.inv(R)
    Qinv = Quaternion.from_rotmat(Rinv)
    a,_ = (Qinv * Q).to_angleaxis()
    assert abs(a) < 1e-15

def test_conj():
    np.set_printoptions(precision=4, suppress=True)

    rotation = 1., np.array([1.,2.,3.])
    Q = Quaternion.from_angleaxis(*rotation)
    R = Q.to_rotmat()
    Rinv = Q.conj().to_rotmat()
    assert np.allclose(np.linalg.inv(R), Rinv)

def test_():
    qx = Quaternion.from_angleaxis(1.2*pi / 2, [1,0,0])
    qz = Quaternion.from_angleaxis(-pi / 4, [0,0,1])
    q = qz * qx
    print q.to_rotation()

