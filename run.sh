#!/bin/bash

set -e

python calib.py --srcpath=/home/msurov/dev/datasets/calib4/*.bmp --pattern=17x10 --saveto=data.json --outdir=/home/msurov/dev/datasets/calib4/dbg/ --iteration=3
