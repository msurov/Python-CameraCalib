
rem python calib.py --srcpath="samples\*.bmp" --outdir="out" --pattern=10x7 --iterations=2 --saveto="out\data.json"
rem python calib.py --srcpath="undistorted\*.bmp" --outdir="out" --pattern=10x7 --iterations=2 --saveto="out\data.json"

python calib.py --srcpath="c:\temp\stereo\samples\unsorted\*.jpg" --pattern=17x10 --saveto="c:\temp\stereo\samples\unsorted\data.txt"
python undistort.py --srcpath="c:\temp\stereo\samples\unsorted\*.jpg" --outdir="c:\temp\stereo\samples\unsorted\out" --calib="c:\temp\stereo\samples\unsorted\data.txt"
