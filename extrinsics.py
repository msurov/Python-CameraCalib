import numpy as np
import cv2
import matplotlib.pyplot as plt
from numpy.linalg import inv, norm, det
import json
from transforms import Quaternion, make_homogen_trans, decompose_homogen_trans


def load_intrinsics(filename):
    with open(filename, 'r') as f:
        data = f.read()
        j = json.loads(data)
        intr = j['intrinsics']
        K = intr['K']
        K = np.array(K, dtype='float32')
        K = np.reshape(K, (3,3))
        distortion = intr['distortion']
        distortion = np.array(distortion, dtype='float32')
        return K, distortion


def locate_points(im):
    points = [np.zeros((0,2))]

    def onclick(event):
        if event.dblclick:
            x = event.xdata
            y = event.ydata
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.01)
            pts = np.array([[x,y]], dtype='float32')
            pts = cv2.cornerSubPix(im, pts, (8,8), (-1,-1), term)
            plt.plot(pts[:,0], pts[:,1], 'x', markersize=20)
            plt.pause(0.01)
            points[0] = np.vstack((points[0], pts))

        if len(points[0]) == 8:
            plt.close()

    def onkeypress(event):
        if event.key == 'escape':
            plt.close()

    plt.imshow(im, cmap='gray', interpolation='nearest')
    fig = plt.gcf()
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    fig.canvas.mpl_connect('key_press_event', onkeypress)

    plt.show()
    if len(points[0]) != 8:
        return None

    return points[0][0:4], points[0][4:8]


def refine_points(im, pts, wnd=10):
    term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.01)
    pts = np.array(pts, dtype='float32')
    pts = cv2.cornerSubPix(im, pts, (wnd, wnd), (-1,-1), term)
    return pts


def load_quads(filename):
    d = {}

    with open(filename, 'r') as f:
        data = f.read()
        j = json.loads(data)
        for imname in j:
            quads = j[imname]['quads']
            quads = [np.array(q, dtype='float32') for q in quads]
            d[imname] = quads

    return d


'''
    load image and points
'''
imgname = '63950398712.bmp'
im = cv2.imread('/home/msurov/dev/datasets/test_calib/' + imgname, 0)
d = load_quads('points.json')

if imgname not in d:
    pts1, pts2 = locate_points(im)
    print pts1
    print pts2
    exit()
else:
    plt.imshow(im, interpolation='nearest', cmap='gray')
    quads = d[imgname]
    pts1 = refine_points(im, quads[0])
    pts2 = refine_points(im, quads[1])

K, distortion = load_intrinsics('data.json')
quad_side = 80.e-3


def normalized(e):
    return e / (norm(e) + 1e-10)


def get_extrinsics(detected_points):
    plane_pts = np.array([
        [0,quad_side],
        [quad_side,quad_side],
        [quad_side,0],
        [0,0],
    ], dtype=np.float32)
    pts = np.reshape(detected_points, (-1, 1, 2))
    pts = np.array(pts, dtype=np.float32)
    pts = cv2.undistortPoints(pts, K, distortion)
    pts = pts[:,0,:]
    P,_ = cv2.findHomography(plane_pts, pts)
    eu = P[:,0]
    ev = P[:,1]
    p0 = P[:,2]
    nu = norm(eu)
    nv = norm(ev)
    print nu, nv, eu.dot(ev)
    k = ev
    P = P / k
    return P[:,2]


def get_extrinsics2(_3d_points, img_points, K, distortion):
    _img_points = np.reshape(img_points, (-1, 1, 2))
    __3d_points = np.reshape(_3d_points, (-1, 1, 3))

    # retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_P3P)
    retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_ITERATIVE)
    # retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_EPNP)
    # retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_UPNP)
    # retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_AP3P)
    # retval, rvec, tvec = cv2.solvePnP(__3d_points, _img_points, K, distortion, flags=cv2.SOLVEPNP_MAX_COUNT)

    q = Quaternion.from_rodrigues(rvec)
    p = np.reshape(tvec, (3,))
    return p, q


_3d_points = np.array([
    [0,quad_side,0],
    [quad_side,quad_side,0],
    [quad_side,0,0],
    [0,0,0],
], dtype=np.float32)

plt.plot(pts1[:,0], pts1[:,1], 'x', markersize=20)
plt.plot(pts2[:,0], pts2[:,1], 'x', markersize=20)

p1,q1 = get_extrinsics2(_3d_points, pts1, K, distortion)
p2,q2 = get_extrinsics2(_3d_points, pts2, K, distortion)
T1 = make_homogen_trans(p1, q1)
T2 = make_homogen_trans(p2, q2)
T21 = inv(T1).dot(T2)
p,q = decompose_homogen_trans(T21)
print p * 100.

# p1 = get_extrinsics(pts1)
# p2 = get_extrinsics(pts2)
# print norm(p2 - p1)


# 
# show
# 

plt.show()
